#!/bin/bash
# PHP tuning
sed -i 's/;opcache.enable_file_override.*/opcache.enable_file_override = On/g' 		/etc/php/7.3/apache2/php.ini
sed -i 's/;opcache.max_accelerated_files.*/opcache.max_accelerated_files=20000/g' 	/etc/php/7.3/apache2/php.ini
sed -i 's/;opcache.interned_strings_buffer.*/opcache.interned_strings_buffer = 8/g' /etc/php/7.3/apache2/php.ini
# Mysql tuning
sed -i 's/query_cache_limit.*/query_cache_limit = 128K/g' /etc/mysql/mariadb.conf.d/50-server.cnf
sed -i 's/query_cache_size.*/query_cache_size   = 32M/g' /etc/mysql/mariadb.conf.d/50-server.cnf
sed -i 's/thread_cache_size.*/thread_cache_size = 80/g' /etc/mysql/mariadb.conf.d/50-server.cnf
sed -i 's/;max_connections.*/max_connections    = 400/g' /etc/mysql/mariadb.conf.d/50-server.cnf
echo   "query_cache_type       = On" 	>> /etc/mysql/mariadb.conf.d/50-server.cnf
echo   "table_open_cache       = 1000"  >> /etc/mysql/mariadb.conf.d/50-server.cnf
echo   "read_buffer_size       = 2M" 	>> /etc/mysql/mariadb.conf.d/50-server.cnf
echo   "read_rnd_buffer_size   = 1M" 	>> /etc/mysql/mariadb.conf.d/50-server.cnf
echo   "join_buffer_size       = 2M" 	>> /etc/mysql/mariadb.conf.d/50-server.cnf
echo   "sort_buffer_size       = 2M" 	>> /etc/mysql/mariadb.conf.d/50-server.cnf
echo   "tmp_table_size         = 32M" 	>> /etc/mysql/mariadb.conf.d/50-server.cnf
echo   "max_heap_table_size    = 32M" 	>> /etc/mysql/mariadb.conf.d/50-server.cnf
echo   "table_definition_cache = 1000" 	>> /etc/mysql/mariadb.conf.d/50-server.cnf
echo   "performance_schema     = OFF" 	>> /etc/mysql/mariadb.conf.d/50-server.cnf